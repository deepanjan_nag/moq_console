﻿using Moq;
using Ninject;
using System;
using System.Collections.Generic;

namespace MoqDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Mock<IColors> mock = new Mock<IColors>();
            mock.Setup(m => m.GetColors).Returns(new List<string> { "Orange", "Blue", "Gren" });

            var kernel = new StandardKernel();
            kernel.Bind<IColors>().ToConstant(mock.Object);

            var realColors = kernel.Get<IColors>();
            realColors.GetColors.ForEach(c => Console.WriteLine(c));
            Console.ReadLine();
        }
    }
    public interface IColors
    {
        List<string> GetColors { get; }
    }
}
